from __future__ import division

__author__ = 'Bix'
import numpy as np
import math
import cmath
import matplotlib.pyplot as plt
from itertools import izip, chain, izip_longest
from decimal import *
import glob
import os, sys
import errno

import time
#import cmath

#This program can only be started after PolyParte1.py.
#
#In fact it takes as input the output of PolyParte1.py, that is the folder
#with the lists of points with coordinates (xBM,yBM), and in the order
#the following two functions are applied.
#
#The first function is Vieta_function that takes every list of points BM,
#turns it into a list of complex numbers, counts the minimum number of points
#present in the lists of the database and, depending on the chosen value of k,
#computes the Vieta function on the lists. k is the length, that you choose,
#for the vector. It is the number of symmetric functions you decide to compute.
#You can modify it inside the definition of Vieta_function.
#
#The output is a folder of lists of complex numbers that are the first k
#elementary symmetric functions of the transformed cornerpoints, i.e.
#(up to the sign) the coefficients of each polynomial having the tranformed
#cornerpoints as roots (Coefficienti_Complessi).
#
#From this folder pairs of lists are given as input to the second function
#for the computation of the distance between coefficient vectors. You can
#choose between different distances by commenting/uncommenting.
#
#The values of these distances are saved in the form of a symmetric square
#matrix (Output).


#initial_time
tempo_iniziale = time.time()


#functions for reading inputs
def files_in(path):
    from os import listdir
    from os.path import isfile, join
    if path == path2:
        return [f for f in listdir(path2) if isfile(join(path2, f))]
    if path == path3:
        return [f for f in listdir(path3) if isfile(join(path3, f))]


#path of input folder
path2='CornerPoint_BM'


def please_apply_2(this_fun, to_every_file, in_this_path2, and_put_the_output_file_in_this_new_directory=None):
    from os import mkdir, path

    directory = and_put_the_output_file_in_this_new_directory
    ret_value = []
    try:
        mkdir(directory)
    except Exception:
        pass
    for input_file in to_every_file:
        if (and_put_the_output_file_in_this_new_directory != None):
            with open(path.join(directory, input_file), "w") as to_output_file:
                on_this_file = open(path.join(in_this_path2, input_file), "r")
                this_fun(on_this_file, to_output_file)
                on_this_file.close()
                to_output_file.close()
        else:
            on_this_file = open(path.join(in_this_path2, input_file), "r")
            ret_value.append(this_fun(on_this_file))
            on_this_file.close()
    return ret_value


#function that compute the coefficients of polinomial from the cornerpoints
#and that applied the new_trasformation to these coefficients
def vieta_compute(l,k,cp_number):
    r=len(l)
    x=[1]

    for h in range(1,k+1):
        x=[a+b*l[h-1] for a,b,d,e in izip(chain([0],x), chain(x,[0]),range(h+1),range(h,-1,-1))]
        #print (x,a,b,d,e)

    y=x[0:k]
    
    for h in range(k+1,r+1):
        y=[a+b*l[h-1] for a,b,d,e in izip(y,chain(y[1:k],[1]),range(h-k,h),range(k,0,-1))]
        #print (y,a,b,d,e)
  
    y.reverse()
    y=new_trasformation(y,k,cp_number)
    return y

#function tha defined the transformation to apply to the coefficient computed
#from the cornerpoint using Viete's formulas described in the paper
def new_trasformation(y,k,cp_number):
    for i in range(0,len(y)):
        #the indexes of the lists start from 0...
        #range do not consider the last written index...
        t=y[i]
        n=float(cp_number)
        t1=(t.real**2+t.imag**2)**(1/(float(i)+2))
        t2=t1*(math.e**(complex(0,cmath.phase(t))))
        t3=t2/n
        y[i]=t3
    #print n
    return y


def complex_list(l):
    c = [complex(i[0], i[1]) for i in l]
    return c


#the first function described in the comment at the beginning of this program
def Vieta_function(in_list, out_file):
    BM_Vector = []
    xBM = []
    yBM = []
    for line in in_list:
        line = line.strip()
        columns = line.split()
        #in the last line of file of CornerPoint_BM folder there is cp_number
        if len(columns)==1:
            cp_number=columns[0]
        else:
            xBM.append(float(columns[2]))
            yBM.append(float(columns[3]))
    #print cp_number
    zBM = complex_list(izip(xBM, yBM))
    #r = 6
    
    k=10
    #k=20
    #k=50
    
    vBM = vieta_compute(zBM, k, cp_number)
    for i in vBM:
        out_file.write(str(i) + "\t")
        out_file.write('\n')
    return BM_Vector


please_apply_2(Vieta_function, files_in(path2), path2, 'Coefficienti_Complessi' )


path3 = 'Coefficienti_Complessi'


def please_apply_3(this_fun, to_every_file, in_this_path3, and_put_the_output_file_in_this_new_directory):
    from os import mkdir, path
    directory = and_put_the_output_file_in_this_new_directory
    try:
     mkdir(directory)
    except Exception: pass
    Distance_Matrix=np.zeros((len(to_every_file),len(to_every_file)))
    Enumerate_Matrix=[e for e in enumerate(to_every_file)]
    for (i, input_file1) in Enumerate_Matrix:
        for (j, input_file2) in Enumerate_Matrix[i:]:
            on_file1 = open(path.join(in_this_path3, input_file1), "r")
            on_file2 = open(path.join(in_this_path3, input_file2), "r")
            Distance_Matrix[i,j] = Distance_Matrix[j,i] = this_fun(on_file1, on_file2)
            on_file1.close()
            on_file2.close()
    to_output_file = open(path.join(directory, 'MatriceDistanze.txt'), 'w')
    
    
    for i in xrange(0,len(to_every_file)):
        for j in xrange(0,len(to_every_file)):
            to_output_file.write(str(Distance_Matrix[i,j]) + '\t' + '\t')
        to_output_file.write('\n')
    to_output_file.close()


#the second function described in the comment at the beginning of this program
def Distance_function(in_listA, in_listB, Out_file=None):
    i = 1
    DistanceVector=0
    for lineA, lineB in izip(in_listA, in_listB):
        
        #the following distance considers all the coefficient
        #with the same importance
        DistanceLine = abs(complex(lineA) - complex(lineB))
        
        ##other possible distances
        ##the following distances attribute to the coefficients a degree
        ##of importance that decreases as the index of these coefficients
        ##increases, in different ways, one from the other
        #DistanceLine = abs(complex(lineA) - complex(lineB))/i
        #DistanceLine = abs(complex(lineA) - complex(lineB))**(1/i)
        
        DistanceVector = DistanceLine + DistanceVector
        i += 1
    if Out_file != None : Out_file.write(str(DistanceVector))
    return DistanceVector


please_apply_3(Distance_function, files_in(path3), path3, 'Distanze')


#final-time
tempo_finale = time.time()
print "Used", str(tempo_finale - tempo_iniziale), "seconds."




