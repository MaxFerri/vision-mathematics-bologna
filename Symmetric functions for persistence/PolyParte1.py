# -*- coding: cp1252 -*-

__author__ = 'Bix'
import numpy as np
import matplotlib.pyplot as plt
from itertools import izip, chain
import glob
import os, sys
import errno

import math
import time


#This program takes as input the folder with the lists of cornerpoints
#and applies to each list the transformation BM which transforms the 
#coordinates of each point from (x,y) to (xBM,yBM).
#(Insert the path to your folder in the relevant line.)
#The active transformation (BM function) is the one named T in the papers
#"Symmetric functions for fast image retrieval with persistent homology"
#and
#"Shortened Persistent Homology for a Biomedical Retrieval System with
#Relevance Feedback".
#You can switch to transformation R by commenting/uncommenting.
#
#In order to have all the files with the same number of points, points
#of coordinates (0,0) are added to each list that has fewer rows
#than max_number_of_lines.
#The new lists are then saved in the folder BMPoints with the same names.

#initial_time
tempo_iniziale = time.time()

#path of input folder Insert here your path
path1='CornerPoint'


#functions for reading inputs
def files_in(path1):
    from os import listdir
    from os.path import isfile, join
    return [f for f in listdir(path1) if isfile(join(path1, f))]


def please_apply_1(this_fun, to_every_file, in_this_path1, and_put_the_output_file_in_this_new_directory=None):
    from os import mkdir, path
    directory = and_put_the_output_file_in_this_new_directory
    ret_value = []
    try:
        mkdir(directory)
    except Exception: pass
    for input_file in to_every_file:
        if (and_put_the_output_file_in_this_new_directory != None):
            with open(path.join(directory, input_file), "w") as to_output_file:
                on_this_file = open(path.join(in_this_path1, input_file), "r")
                this_fun(on_this_file, to_output_file)
                on_this_file.close()
                to_output_file.close()
        else:
            on_this_file = open(path.join(in_this_path1, input_file), "r")
            ret_value.append(this_fun(on_this_file))
            on_this_file.close()
    return ret_value


#function to count lines in the input lists
def count_lines(in_list):
    lines = 0
    for line in in_list:
        lines += 1
    return lines


max_number_of_lines = max(please_apply_1(count_lines, files_in(path1), path1))
print max_number_of_lines


#BM function
def BM_function(in_list, out_file):
    BM_list=[]
    i=1
    for line in in_list:
        line = line.strip()
        
        columns = line.split()
        x = float(columns[0])
        y = float(columns[1])
        a = np.sqrt(x**2+y**2)
        
        #TRANFORMATION T
        xBM = (y-x)*(np.cos(a) - np.sin(a))/2
        yBM = (y-x)*(np.cos(a) + np.sin(a))/2
        
        ##other possible transformation
        ##TRANSFORMATION R
        #theta=(math.pi)*(x+y)
        #xBM=((y-x)/np.sqrt(2))*(np.cos(theta))
        #yBM=((y-x)/np.sqrt(2))*(np.sin(theta))
        
        BM_list.append(['p', i, xBM, yBM])
        out_file.write('p' + "\t" + str(i) + "\t" + str(xBM) + "\t" + str(yBM) + "\n")
        i=i+1
    #in cp_number there is the number of cornerpoints related to the
    #considered image
    cp_number=len(BM_list)
    if len(BM_list)<max_number_of_lines:
        for j in xrange(len(BM_list)+1, max_number_of_lines+1):
            BM_list.append(['p', j, 0, 0])
            out_file.write('p' + "\t" + str(j) + "\t" + str(0.0) + "\t" + str(0.0) + "\n")
    #as the least line of the file there is the number of cornerpoints of
    #that image
    out_file.write(str(cp_number)+"\n")
    #print cp_number
    return BM_list


please_apply_1(BM_function, files_in(path1), path1,'CornerPoint_BM')


#final_time
tempo_finale = time.time()
print "Used", str(tempo_finale - tempo_iniziale), "seconds."



